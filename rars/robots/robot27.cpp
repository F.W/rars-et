/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;
//--------------------------------------------------------------------------
//                           Class Robot27
//--------------------------------------------------------------------------

class Robot27 : public Driver
{
public:
    // Konstruktor
    Robot27()
    {
        // Der Name des Robots
        m_sName = "Robot27";
        // Namen der Autoren
        m_sAuthor = "Fabian";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oWHITE;
        m_sBitmapName2D = "car_lblue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
    con_vec result = CON_VEC_EMPTY;

    double lane1;
    double lane2;
    double alpha;
    double breite;
    double geschwindigkeit;

    if( s.starting )
    {
      result.fuel_amount = MAX_FUEL;     // Full fuel when starting
    }

    // Berechnung Streckenbreite
    breite= s.to_lft+s.to_rgt;

    // Unstuck-Funktion
    if( stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc ) )
    {
      return result;
    }

    // 1. Berechnungen der Spur in Kurven
    if (s.cur_rad==0)
        {
        lane1 = breite/2;
        lane2 = breite/2;
        geschwindigkeit =90;
        }
    else if (s.cur_rad > 0)
        {
        lane1 = 0.05 * breite;
        lane2 = 0.95 * breite;
        geschwindigkeit =85;
        }
    else if (s.cur_rad < 0)
        {
        lane1 = 0.95 * breite;
        lane2 = 0.05 *breite;
        geschwindigkeit= 85;
        }

    // Berechnung des Lankwinkels in den Kurven ( evtl. Auslagerung und Optimierung)
    //         Winkelberechnung     +          Spurkorrektur
    alpha = -asin(s.vn/s.v)*2*3.1415+((s.to_lft-lane1)-(s.to_rgt-lane2))/(2*breite);

    // bisher: Konstante Geschwindigkeit fahren
    result.vc = geschwindigkeit;          // Tempo übergabe
    result.alpha = alpha;                // Winkel alpha übergeben


    // Boxenstopp, wenn Sprit zu wenig oder zuviel Schaden
    if( s.fuel< 7.5 || s.damage > 20000)
    {
      result.request_pit   = 1;
      result.repair_amount = s.damage;
      result.fuel_amount = MAX_FUEL;
    }

    return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot27Instance()
{
    return new Robot27();
}
